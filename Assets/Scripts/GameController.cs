﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [SerializeField] KeyCode keyLeft = KeyCode.A;
    [SerializeField] KeyCode keyRight = KeyCode.D;
    [SerializeField] KeyCode keyRotate = KeyCode.R;
    [SerializeField] KeyCode keySpeed = KeyCode.Space;

    static GameController Instance = null;

    public enum ControllAction
    {
        None,
        Left,
        Right,
        Speed,
        Rotate
    }

    private ControllAction Action;

    private float TimerControllerFigure = 0.0f;       // время
    private float SpeedControllerFigure = 0.1f;       // текущая скорость фигуры
    


    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        Action = ControllAction.None;

        if (Input.GetKeyDown(keyLeft))
        {
            Action = ControllAction.Left;
        }

        if (Input.GetKeyDown(keyRight))
        {
            Action = ControllAction.Right;
        }

        TimerControllerFigure += Time.deltaTime;
        if (TimerControllerFigure >= SpeedControllerFigure) {
            if (Input.GetKey(keyLeft) || Input.GetKey(KeyCode.LeftArrow)) {
                Action = ControllAction.Left;
            }
            if (Input.GetKey(keyRight) || Input.GetKey(KeyCode.RightArrow)) {
                Action = ControllAction.Right;
            }
            TimerControllerFigure -= SpeedControllerFigure;
        }

        if (Input.GetKeyDown(keyRotate) || Input.GetKeyDown(KeyCode.UpArrow)) {
            Action = ControllAction.Rotate;
        }

        if (Input.GetKey(keySpeed) || Input.GetKey(KeyCode.Space))
        {
            Action = ControllAction.Speed;
        }
    }

    public static GameController GetInstance()
    {
        return Instance;
    }

    public ControllAction GetAction()
    {
        return Action;
    }

}
