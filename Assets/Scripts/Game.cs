﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;



public class Game : MonoBehaviour {
  
    [System.Serializable]
    public struct FigureSettings
    {
        [TextArea]
        public String figure;
        public Sprite sprite;

        public FigureSettings(string figure) : this()
        {
            this.figure = figure;
            this.sprite = null;
        }
    }

    public FigureSettings[] ListFigures = new FigureSettings[]
    { 
        new FigureSettings("0000\n" +
                           "0111\n" +
                           "0100\n" +
                           "0000"),
        new FigureSettings("0000\n" +
                           "1111\n" +
                           "0000\n" +
                           "0000"),
        new FigureSettings("0000\n" +
                           "0110\n" +
                           "0110\n" +
                           "0000"),
        new FigureSettings("0000\n" +
                           "1100\n" +
                           "0110\n" +
                           "0000"),
        new FigureSettings("0000\n" +
                           "0100\n" +
                           "1110\n" +
                           "0000"),
    };

    private float oldCameraAspect = 0;

    [SerializeField] private GameObject prefab;


    private GameObject widgetGameOver;


    Table TableGame;
    Table TablePreview;


    private int Cols = 10;
    private int Rows = 18;
    
    private int Level = 1;
    private int Lines = 0;

    public static int nextIndexFigure = -1;


    private float timerMoveFigure = 0.0f;       // время
    private float speedMoveFigure = 0.5f;       // текущая скорость фигуры
    private float AccelerationSpeedFigure = 5.0f;       // Скорость ускорения

    private TextMesh TextLines;
    private TextMesh TextLevel;


    // Start is called before the first frame update
    void Start()
    {
        widgetGameOver = GameObject.Find("GameOver");
        widgetGameOver.SetActive(false);

        TableGame = new Table(GameObject.Find("GameTable"), Cols, Rows);
        TablePreview = new Table(GameObject.Find("PreviewTable"), 4, 4);

        TextLines = GameObject.Find("TextLines").GetComponent<TextMesh>();
        TextLevel = GameObject.Find("TextLevel").GetComponent<TextMesh>();

        RedrawCell();
        CreateNewFigure();


        TextLines.text = "Lines: 0";
        TextLevel.text = "Level: 0";
    }


    // Update is called once per frame
    void Update()
    {
        if (Camera.main.aspect != oldCameraAspect)
        {
            oldCameraAspect = Camera.main.aspect;

            RedrawCell();
        }

        TextLevel.text = "Level: " + Lines/10;


        if (GameController.GetInstance().GetAction() == GameController.ControllAction.Left)
        {
            TableGame.FigureMove(new CubePos(-1, 0));
        }

        if (GameController.GetInstance().GetAction() == GameController.ControllAction.Right)
        {
            TableGame.FigureMove(new CubePos(1, 0));
        }

        if (GameController.GetInstance().GetAction() == GameController.ControllAction.Rotate)
        {
            TableGame.FigureRotate();
        }

        float AddingSpeed = 1.0f;
        if (GameController.GetInstance().GetAction() == GameController.ControllAction.Speed)
        {
            AddingSpeed = AccelerationSpeedFigure;
        }
        
        timerMoveFigure += Time.deltaTime * AddingSpeed;

        if (timerMoveFigure >= speedMoveFigure)
        {
            if (TableGame.FigureIsDone())
            {
                CheckLines();

                if (TableGame.IsEndGame())
                {
                    widgetGameOver.SetActive(true);
                }
                else
                {
                    CreateNewFigure();
                }
            }
            else
            {
                TableGame.FigureMove(new CubePos(0, 1));
                timerMoveFigure -= speedMoveFigure;
            }            
        }
    }

    private void RedrawCell()
    {
        float sizeBlock = 32.0f;
        float aspectBlock = 1.0f;

        float sizeCameraWidth = Camera.main.orthographicSize * Camera.main.aspect * 2;
        float maxSize = (sizeCameraWidth - 16) / 10;

        if (maxSize < sizeBlock)
        {
            aspectBlock = maxSize / sizeBlock;
        }

        // Получение координат для размещения в середину игрового поля
        float startX = (-sizeCameraWidth / 2f) + ((sizeCameraWidth - (10 * sizeBlock * aspectBlock)) / 2f) + ((sizeBlock * aspectBlock) / 2f);
        float startY = 16 - (Camera.main.orthographicSize - sizeBlock * aspectBlock / 2f) + 17 * sizeBlock * aspectBlock;

        TableGame.Move(startX, startY);
        TableGame.SetScale(aspectBlock);

        TablePreview.Move((5 * sizeBlock * aspectBlock) - (4 * 16 * aspectBlock), startY + 5* (16 * aspectBlock));
        TablePreview.SetScale(aspectBlock / 2);



        GameObject.Find("TextLines").GetComponent<Transform>().position = new Vector3(startX, startY + 80, 0);
        //GameObject.Find("TextLines").GetComponent<Transform>().localScale = new Vector3(21 , 21 , 1);
        GameObject.Find("TextLevel").GetComponent<Transform>().position = new Vector3(startX, startY + 50, 0);
        //GameObject.Find("TextLevel").GetComponent<Transform>().localScale = new Vector3(21 , 21 , 1);

    }

    public void CreateNewFigure() 
    {
        if (nextIndexFigure == -1) nextIndexFigure = UnityEngine.Random.Range(0, ListFigures.Length);

        TableGame.SetFigure(ListFigures[nextIndexFigure].figure, ListFigures[nextIndexFigure].sprite, new CubePos(4, -4));

        nextIndexFigure = UnityEngine.Random.Range(0, ListFigures.Length);

        TablePreview.ClearAllCube();
        TablePreview.SetFigure(ListFigures[nextIndexFigure].figure, ListFigures[nextIndexFigure].sprite, new CubePos(0, 0));
        TablePreview.FigureSetPosition(new CubePos(0, 0));
    }

 
    public int GetLevel()
	{
        return Lines / 10;
    }

    private void CheckLines(){
        for (int y = 0; y < this.Rows; y++)
        {
            bool isFillLines = true;
            for (int x = 0; x < this.Cols; x++)
            {
                if (TableGame.GlassGetCube(x, y)==null)
                {
                    isFillLines = false;
                    break;
                }
            }

            if (isFillLines)
            {
                Lines++;

                TextLines.text = "Lines: "+Lines;
                speedMoveFigure = (0.5f - 0.05f * (Lines / 10));

                for (int x = 0; x < this.Cols; x++)
                {
                    TableGame.GlassGetCube(x, y).Remove();
                    TableGame.GlassRemoveCube(new CubePos(x, y));
                }

                for (int y2 = y-1; y2 > -4; y2--)
                {
                    for (int x = 0; x < this.Cols; x++) 
                    {
                        Cube cube = TableGame.GlassGetCube(new CubePos(x, y2));
                        if (cube != null) 
                        {
                            cube.Move(new CubePos(0, 1));
                            TableGame.GlassSwapCube(new CubePos(x,y2), cube.GetPosition());
                        }

                    }
                }
            }

        }
    }


}

