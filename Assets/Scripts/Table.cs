﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Table {

    private GameObject prefab;
    private GameObject parent;
    
    private Cube[,] Glass;

    public int Cols { get; set; }
    public int Rows { get; set; }

    private float SizeCube = 32.0f;

    private Figure Figure;

    // Добавочные элементы, для минус координат
    private int additional_hidden_rows = 5;

    // Start is called before the first frame update
    public Table(GameObject parent,int Cols,int Rows)
    {
        this.Cols = Cols;
        this.Rows = Rows;

        Glass = new Cube[Cols, Rows + additional_hidden_rows];

        prefab = Resources.Load("SquareCell") as GameObject;
        this.parent = parent;

        CreateGrid(this.Cols, this.Rows);

        // Создания фигуры
        Figure = new Figure(this);

    }


    // create the grid
    public void CreateGrid(int Cols,int Rows)
    {
        for (int x = 0; x < Cols; x++)
        {
            for (int y = 0; y < Rows; y++)
            {
                GameObject Cell = GameObject.Instantiate(prefab);
                Cell.transform.parent = this.parent.transform;
                Cell.transform.localPosition = new Vector3(x* SizeCube, -y* SizeCube, 0);
                Cell.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
        }
    }


    public Cube CreateCube(int x, int y, Sprite sprite)
    {
        Cube cube = GameObject.Instantiate(prefab, this.parent.transform).GetComponent<Cube>();
        cube.SetPosition(new CubePos(x, y));
        cube.SetSprite(sprite);
        GlassSetCube(cube, new CubePos(x, y));

        return cube;
    }

    public void ClearAllCube()
	{
        for (int x = 0; x < Cols; x++)
        {
            for (int y = 0; y < Rows; y++)
            {
                Cube cube = GlassGetCube(x, y);
                if (cube != null)
                {
                    cube.Remove();
                    GlassRemoveCube(new CubePos(x, y));
                }
            }
        }
    }

    public bool IsEndGame()
    {
        for (int i = 0; i < Cols; i++)
        {
            if (GlassGetCube(i, -1) != null)
            {
                return true;
            }
        }

        return false;
    }


    public void FigureMove(CubePos direction)
	{
        Figure.Move(direction);
    }

    public void FigureSetPosition(CubePos pos)
    {
        Figure.SetPosition(pos);
    }

    public void FigureRotate()
	{
        Figure.Rotate();
    }

    public bool FigureIsDone()
	{
        return Figure.IsDone();
	}

    public void SetFigure(string figure_view, Sprite sprite, CubePos position)
	{
        Figure.SetFigure(figure_view, sprite, position);
    }

    public void Move(float x,float y)
    {
        this.parent.transform.position = new Vector3(x, y, 0);
    }

    public void SetScale(float scale)
    {
        this.parent.transform.localScale = new Vector3(1.0f * scale, 1.0f * scale, 1.0f) ;
    }


    public void GlassSetCube(Cube cube, CubePos pos)
    {
        Glass[pos.x, pos.y + additional_hidden_rows] = cube;
    }

    public Cube GlassGetCube(CubePos pos)
    {
        return Glass[pos.x, pos.y + additional_hidden_rows];
    }
    public Cube GlassGetCube(int x, int y)
    {
        return GlassGetCube(new CubePos(x, y));
    }

    public void GlassRemoveCube(CubePos pos)
    {
        Glass[pos.x, pos.y + additional_hidden_rows] = null;
    }

    public void GlassSwapCube(CubePos pos1, CubePos pos2)
    {
        Cube tmpCube = Glass[pos1.x, pos1.y + additional_hidden_rows];
        Glass[pos1.x, pos1.y + additional_hidden_rows] = Glass[pos2.x, pos2.y + additional_hidden_rows];
        Glass[pos2.x, pos2.y + additional_hidden_rows] = tmpCube;
    }
}
