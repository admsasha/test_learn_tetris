﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Figure
{
    private List<Cube> listObjs;
    private Table table;

    CubePos CurrentPos;

    public Figure(Table table)
    {
        listObjs = new List<Cube>();
        this.table = table;
    }


    public void SetFigure(string figure, Sprite sprite,CubePos position)
    {
        figure = figure.Replace("\n","");
        listObjs.Clear();
        CurrentPos = position;

        // Создание сетки "Следующий элемент"
        for (int x = 0; x < 4; x++)
        {
            for (int y = 0; y < 4; y++)
            {
                if (figure[y * 4 + x] == '1')
                {
                    Cube cube = table.CreateCube(CurrentPos.x+ x, CurrentPos.y+ y, sprite);
                    listObjs.Add(cube);
                }
            }
        }
    }




    // поворачивание фигуры
    public void Rotate()
    {
        if (!CanRotate()) return;

        // Удаляем все старые позиции
        for (int i = 0; i < listObjs.Count; i++) this.table.GlassRemoveCube(listObjs[i].GetPosition());        

        // Разворачиваем
        for (int i = 0; i < listObjs.Count; i++)
        {
            CubePos pos = listObjs[i].GetPosition();
            CubePos localPos = pos - CurrentPos;
            listObjs[i].SetPosition(new CubePos (CurrentPos.x+(3- localPos.y), CurrentPos.y+localPos.x));
            this.table.GlassSetCube(listObjs[i], listObjs[i].GetPosition());
        }
    }

    public void Move(CubePos pos)
    {
        if (CanMove(pos) == false) return;

        CurrentPos += pos;

        // Удаляем все старые позиции
        for (int i = 0; i < listObjs.Count; i++) this.table.GlassRemoveCube(listObjs[i].GetPosition());

        for (int i = 0; i < listObjs.Count; i++)
        {
            listObjs[i].Move(new CubePos(pos.x, pos.y));
            this.table.GlassSetCube(listObjs[i], listObjs[i].GetPosition());
        }

    }

    public void SetPosition(CubePos pos)
	{
        Move(pos - CurrentPos);
    }

    public bool IsDone()
    {
        return !CanMove(new CubePos(0, 1));
    }


    private bool CanRotate()
    {
        for (int i = 0; i < listObjs.Count; i++) 
        {
            CubePos pos = listObjs[i].GetPosition();
            CubePos localPos = pos - CurrentPos;
            CubePos newPos = new CubePos(CurrentPos.x + (3 - localPos.y), CurrentPos.y + localPos.x);

            if (newPos.y >= table.Rows) return false;
            if (newPos.x < 0) return false;
            if (newPos.x >= table.Cols) return false;

            Cube obj = this.table.GlassGetCube(newPos);
            if (obj != null && listObjs.IndexOf(obj) == -1) return false;

        }

        return true;
    }

    private bool CanMove(CubePos direction) 
    {
        for (int i = 0; i < listObjs.Count; i++)
        {
            CubePos newPos = listObjs[i].GetPosition() + direction;
            
            if (newPos.y >= table.Rows) return false;
            if (newPos.x < 0) return false;
            if (newPos.x >= table.Cols) return false;
            
            Cube obj = this.table.GlassGetCube(newPos);
            if (obj!=null && listObjs.IndexOf(obj) == -1){
                return false;
            }
        }
        return true;
    }

}

