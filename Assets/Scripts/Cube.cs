﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CubePos 
{
    public int x;
    public int y;

    public CubePos(int x,int y) 
    {
        this.x = x;
        this.y = y;
    }

    public static CubePos operator+ (CubePos c1, CubePos c2) 
    {
        return new CubePos(c1.x + c2.x, c1.y + c2.y);
    }

    public static CubePos operator- (CubePos c1, CubePos c2) 
    {
        return new CubePos(c1.x - c2.x, c1.y - c2.y);
    }
};

public class Cube : MonoBehaviour 
{
    private float SizeCube = 32.0f;     // Размер кубика
    private CubePos CurPos = new CubePos(-1,-1);
    private SpriteRenderer spriteRenderer = null;

    void Awake() 
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }

    // Установить позицию в координатах стакана
    public void SetPosition(CubePos pos)
    {
        CurPos = pos;
        transform.localPosition = new Vector3(CurPos.x * SizeCube, -CurPos.y * SizeCube, 0);
        if (CurPos.y < 0) {
            spriteRenderer.enabled = false;
        } else {
            spriteRenderer.enabled = true;
        }
    }

    public CubePos GetPosition()
    {
        return CurPos;
    }

    public void SetSprite(Sprite sprite)
    {
        spriteRenderer.sprite = sprite;
    }

    public void Move(CubePos pos)
    {
        SetPosition(CurPos + pos);
    }

    public void Remove()
    {
        Destroy(gameObject);
    }
}
